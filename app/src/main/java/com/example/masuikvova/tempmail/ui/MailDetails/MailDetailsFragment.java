package com.example.masuikvova.tempmail.ui.MailDetails;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.masuikvova.tempmail.MailApplication;
import com.example.masuikvova.tempmail.R;
import com.example.masuikvova.tempmail.di.DaggerPresenterComponent;
import com.example.masuikvova.tempmail.di.PresenterComponent;
import com.example.masuikvova.tempmail.helper.Utils;
import com.example.masuikvova.tempmail.models.Email;
import com.example.masuikvova.tempmail.ui.Base.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Vova on 12.03.2018.
 */

public class MailDetailsFragment extends BaseFragment implements MailDetailsView {
    private static final String KEY = "email";
    @BindView(R.id.home)
    ImageButton home;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvSign)
    TextView tvSign;
    @BindView(R.id.tvContent)
    TextView tvContent;
    @BindView(R.id.tvSubject)
    TextView tvSubject;
    @BindView(R.id.pbLoader)
    ProgressBar pbLoader;
    @BindView(R.id.empty)
    FrameLayout empty;
    @BindView(R.id.emptyImg)
    ImageView emptyImg;

    private Unbinder unbinder;
    @Inject
    MailDetailsPresenter presenter;
    private Email email;
    private Animation scaleAnim;

    public static MailDetailsFragment newInstance(Email email) {
        MailDetailsFragment fragment = new MailDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY, email);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        email = getArguments().getParcelable(KEY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rooView = inflater.inflate(R.layout.fragment_details, container, false);
        unbinder = ButterKnife.bind(this, rooView);
        initPresenter();
        initView();
        presenter.onViewAttached(this);
        presenter.getEmailDetails(email.getId());
        return rooView;
    }

    private void initPresenter() {
        PresenterComponent presenterComponent = DaggerPresenterComponent.builder().appComponent(MailApplication.get(getActivity()).getAppComponent()).build();
        presenterComponent.inject(this);
    }

    private void initView() {
        scaleAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_scale);
        scaleAnim.setInterpolator(time -> (float) (-1 * Math.pow(Math.E, -time / 0.4) * Math.cos(20 * time) + 1));
        home.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_back));
        setEmailData(email);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onViewDetached();
        unbinder.unbind();
    }

    @Override
    public void showLoading(boolean show) {
        if (isAdded()) {
            pbLoader.setVisibility(show ? View.VISIBLE : View.GONE);
            tvContent.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void setEmailData(Email email) {
        tvDate.setText(Utils.parseDate(email.getData()));
        tvSign.setText(Utils.getSign(email.getFrom()));
        tvEmail.setText(Utils.extractMail(email.getFrom()));
        tvTitle.setText(Utils.formatTitle(email.getFrom()));
        tvSubject.setText(email.getSubject());
    }

    @Override
    public void setMailText(String text) {
        if (isAdded()) {
            tvContent.setText(Html.fromHtml(text));
        }
    }

    @Override
    public void showLoginScreen() {
        if (isAdded())
            getRouter().showLoginScreen();
    }

    @Override
    public void showMailList() {
        if (isAdded())
            getRouter().backScreen();
    }

    @Override
    public void showError(String msg) {
        super.showError(msg);
        empty.setVisibility(View.VISIBLE);
        emptyImg.startAnimation(scaleAnim);
    }

    @OnClick(R.id.home)
    void backToList() {
        getRouter().backScreen();
    }
}
