package com.example.masuikvova.tempmail.ui.MailList;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.masuikvova.tempmail.models.Email;
import com.example.masuikvova.tempmail.ui.Navigation.Router;

import java.util.List;

/**
 * Created by Vova on 06.03.2018.
 */

public class MailListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Email> data;
    private Router router;

    public MailListAdapter(Router router) {
        this.router = router;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return MailListViewHolder.create(parent, router);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Email email = data.get(position);
        ((MailListViewHolder) holder).init(email);
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    void setData(List<Email> data) {
        this.data = data;
        notifyDataSetChanged();
    }
}
