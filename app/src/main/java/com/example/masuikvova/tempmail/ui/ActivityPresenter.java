package com.example.masuikvova.tempmail.ui;

import com.example.masuikvova.tempmail.helper.IScheduler;
import com.example.masuikvova.tempmail.helper.Utils;
import com.example.masuikvova.tempmail.models.BaseModel;
import com.example.masuikvova.tempmail.repository.MailRepository;
import com.example.masuikvova.tempmail.ui.Base.BasePresenter;
import com.example.masuikvova.tempmail.ui.Base.BaseView;

/**
 * Created by Vova on 14.03.2018.
 */

public class ActivityPresenter extends BasePresenter {
    private ActivityView view;

    public ActivityPresenter(MailRepository repository, IScheduler scheduler) {
        super(repository, scheduler);
    }

    @Override
    public void onViewAttached(BaseView baseView) {
        view = (ActivityView) baseView;
        showScreen();
    }

    private void showScreen() {
        Utils.log("Account" + repository.getAccount().toString());
        Utils.log("Time saved " + Utils.formatLogTime(repository.getExpirationTime()) + " current " + Utils.formatLogTime(System.currentTimeMillis()));
        if (repository.getAccount().isEmpty() || repository.getExpirationTime() < System.currentTimeMillis())
            view.showLogin();
        else
            view.showList();
    }

    public void onResume() {
        repository.getExpirationTime(repository.getAccount().getKey())
                .compose(scheduler.getThreadTransformer())
                .subscribe(status -> Utils.log("Time updated " + status.getLiveTime())
                        , throwable -> {
                            if (throwable.getMessage().equals(BaseModel.KEY_NOT_FOUND))
                                view.showLogin();
                        });
    }

    @Override
    public void onViewDetached() {
        view = null;
    }
}
