package com.example.masuikvova.tempmail.ui.MailList;

import com.example.masuikvova.tempmail.models.Email;
import com.example.masuikvova.tempmail.ui.Base.BaseView;

import java.util.List;

/**
 * Created by Vova on 01.03.2018.
 */

public interface MailListView extends BaseView {
    void showMessage(String msg);

    void showMailList(List<Email> data);

    void showEmptyView(boolean show);

    void showLoginScreen();

    void initTimer(long time);
}
