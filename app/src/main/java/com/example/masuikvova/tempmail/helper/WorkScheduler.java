package com.example.masuikvova.tempmail.helper;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Vova on 10.03.2018.
 */

public class WorkScheduler implements IScheduler {

    public <T> Observable.Transformer<T, T> getThreadTransformer() {
        return observable -> observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
