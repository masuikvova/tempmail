package com.example.masuikvova.tempmail.ui.Login;

import com.example.masuikvova.tempmail.TestUtils.MockScheduler;
import com.example.masuikvova.tempmail.TestUtils.TestRepository;
import com.example.masuikvova.tempmail.repository.MailRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by Vova on 27.03.18.
 */
public class LoginPresenterTest {
    private MailRepository repositorySuccess;
    private MailRepository repositoryFail;
    private MockScheduler testScheduler;

    @Before
    public void setUp() {
        repositoryFail = new TestRepository(false, false);
        repositorySuccess = new TestRepository(true, false);
        testScheduler = new MockScheduler();
    }

    @Test
    public void wrongName() {
        LoginPresenter presenter = new LoginPresenter(repositorySuccess, testScheduler);
        LoginView view = Mockito.mock(LoginView.class);

        presenter.onViewAttached(view);
        presenter.createEmailBox("test@test");

        Mockito.verify(view).showError(Mockito.anyString());
    }

    @Test
    public void successCreateMail() {
        LoginPresenter presenter = new LoginPresenter(repositorySuccess, testScheduler);
        LoginView view = Mockito.mock(LoginView.class);

        presenter.onViewAttached(view);
        presenter.createEmailBox("");

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).onCreationSuccess();
    }

    @Test
    public void successCreateMailWithName() {
        LoginPresenter presenter = new LoginPresenter(repositorySuccess, testScheduler);
        LoginView view = Mockito.mock(LoginView.class);

        presenter.onViewAttached(view);
        presenter.createEmailBox("box");

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).onCreationSuccess();
    }

    @Test
    public void failCreateMail() {
        LoginPresenter presenter = new LoginPresenter(repositoryFail, testScheduler);
        LoginView view = Mockito.mock(LoginView.class);

        presenter.onViewAttached(view);
        presenter.createEmailBox("");

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).showError(TestRepository.TEST_ERROR);
    }
}