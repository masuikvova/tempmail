package com.example.masuikvova.tempmail.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.masuikvova.tempmail.MailApplication;
import com.example.masuikvova.tempmail.R;
import com.example.masuikvova.tempmail.di.DaggerPresenterComponent;
import com.example.masuikvova.tempmail.di.PresenterComponent;
import com.example.masuikvova.tempmail.ui.Navigation.Router;
import com.example.masuikvova.tempmail.ui.Navigation.RouterImp;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements ActivityView {

    private Router router;
    @Inject
    ActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PresenterComponent presenterComponent = DaggerPresenterComponent.builder().appComponent(MailApplication.get(this).getAppComponent()).build();
        presenterComponent.inject(this);
        router = new RouterImp(this);
        presenter.onViewAttached(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onDestroy() {
        presenter.onViewDetached();
        super.onDestroy();
    }

    public Router getRouter() {
        return router;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1)
            finish();
        else
            super.onBackPressed();
    }

    @Override
    public void showError(String msg) {

    }

    @Override
    public void showList() {
        router.showMailList();
    }

    @Override
    public void showLoading(boolean show) {
    }

    @Override
    public void showLogin() {
        router.showLoginScreen();
    }
}
