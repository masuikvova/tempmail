package com.example.masuikvova.tempmail.ui;

import com.example.masuikvova.tempmail.TestUtils.MockScheduler;
import com.example.masuikvova.tempmail.TestUtils.TestRepository;
import com.example.masuikvova.tempmail.helper.IScheduler;
import com.example.masuikvova.tempmail.repository.MailRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class ActivityPresenterTest {

    private ActivityView view;
    private IScheduler scheduler;

    @Before
    public void setUp() {
        view = Mockito.mock(ActivityView.class);
        scheduler = new MockScheduler();
    }

    @Test
    public void existValidAccount() {
        MailRepository repository = new TestRepository(true, false);
        ActivityPresenter presenter = new ActivityPresenter(repository, scheduler);

        presenter.onViewAttached(view);
        Mockito.verify(view).showList();
    }

    @Test
    public void notValidAccount() {
        MailRepository repository = new TestRepository(false, false);
        ActivityPresenter presenter = new ActivityPresenter(repository, scheduler);

        presenter.onViewAttached(view);
        Mockito.verify(view).showLogin();
    }

    @Test
    public void timeExpire() {
        MailRepository repository = new TestRepository(true, true);
        ActivityPresenter presenter = new ActivityPresenter(repository, scheduler);

        presenter.onViewAttached(view);
        Mockito.verify(view).showList();


        ((TestRepository) repository).setup(false, true);
        presenter.onResume();
        Mockito.verify(view).showLogin();
    }

    @Test
    public void timeNotExpire() {
        MailRepository repository = new TestRepository(true, false);
        ActivityPresenter presenter = new ActivityPresenter(repository, scheduler);

        presenter.onViewAttached(view);
        Mockito.verify(view).showList();

        presenter.onResume();
        Mockito.verifyZeroInteractions(view);
    }

}