package com.example.masuikvova.tempmail.ui.Login;

import com.example.masuikvova.tempmail.helper.IScheduler;
import com.example.masuikvova.tempmail.helper.Utils;
import com.example.masuikvova.tempmail.models.Account;
import com.example.masuikvova.tempmail.repository.MailRepository;
import com.example.masuikvova.tempmail.ui.Base.BasePresenter;
import com.example.masuikvova.tempmail.ui.Base.BaseView;

import rx.Observable;

/**
 * Created by Vova on 27.02.2018.
 */

public class LoginPresenter extends BasePresenter {

    private LoginView view;

    public LoginPresenter(MailRepository repository, IScheduler scheduler) {
        super(repository, scheduler);
    }

    public void createEmailBox(String emailName) {
        if (!Utils.isEmpty(emailName) && emailName.contains("@"))
            view.showError("The email name can not contain @ !");
        else
            getObservable(emailName)
                    .compose(scheduler.getThreadTransformer())
                    .doOnSubscribe(() -> view.showLoading(true))
                    .doAfterTerminate(() -> view.showLoading(false))
                    .subscribe(account -> {
                        Utils.log(account.toString());
                        if (!account.hasError())
                            view.onCreationSuccess();
                        else
                            view.showError(account.getErrorMsg());
                    }, throwable -> view.showError(throwable.getMessage()));
    }

    @Override
    public void onViewAttached(BaseView baseView) {
        this.view = (LoginView) baseView;
    }

    @Override
    public void onViewDetached() {
        view = null;
    }

    private Observable<Account> getObservable(String emailName) {
        if (Utils.isEmpty(emailName))
            return repository.createMainBox();
        else
            return repository.createMainBox(emailName);
    }
}
