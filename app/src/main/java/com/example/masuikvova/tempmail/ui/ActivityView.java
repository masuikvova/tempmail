package com.example.masuikvova.tempmail.ui;


import com.example.masuikvova.tempmail.ui.Base.BaseView;

public interface ActivityView extends BaseView {
    void showList();

    void showLogin();
}
