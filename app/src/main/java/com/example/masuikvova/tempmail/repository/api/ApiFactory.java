package com.example.masuikvova.tempmail.repository.api;

import android.support.annotation.NonNull;

import com.example.masuikvova.tempmail.BuildConfig;
import com.example.masuikvova.tempmail.models.EmailList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Vova on 21.02.2018.
 */

public class ApiFactory {
    private static final String BASE_URL = "http://post-shift.ru/";
    private static OkHttpClient client;

    @NonNull
    public static MailService createService() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(EmailList.class, new MailListDeserializer())
                .create();
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
                .create(MailService.class);
    }

    @NonNull
    private static OkHttpClient getClient() {
        if (client == null)
            client = buildClient();
        return client;
    }

    @NonNull
    private static OkHttpClient buildClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);
        }
        return builder.build();
    }
}
