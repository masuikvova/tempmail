package com.example.masuikvova.tempmail.ui.Login;

import com.example.masuikvova.tempmail.ui.Base.BaseView;

/**
 * Created by Vova on 27.02.2018.
 */

public interface LoginView extends BaseView {
    void onCreationSuccess();
}
