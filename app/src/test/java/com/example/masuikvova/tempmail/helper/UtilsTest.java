package com.example.masuikvova.tempmail.helper;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vova on 23.03.18.
 */
public class UtilsTest {
    private String from = "From user name <example@gmail.com>";
    private String title = "From user name ";
    private String email = "example@gmail.com";
    private String dateRaw = "Fri, 23 Mar 2018 14:31:52 +0000";
    private String dateFormatted ="23:03:18 16:31";
    @Test
    public void getSign() throws Exception {
        String name ="MailName";
        Assert.assertEquals(Utils.getSign(name),"M");
        Assert.assertNotEquals(Utils.getSign(name),"N");
        Assert.assertEquals(Utils.getSign(null),"");
    }

    @Test
    public void formatTitle() throws Exception {
        Assert.assertEquals(Utils.formatTitle(from), title);
        Assert.assertNotEquals(Utils.formatTitle(from), "some other string");
        Assert.assertNotEquals(Utils.formatTitle(from), email);
        Assert.assertEquals(Utils.formatTitle(null), null);
    }

    @Test
    public void extractMail() throws Exception {
        Assert.assertEquals(Utils.extractMail(from), email);
        Assert.assertNotEquals(Utils.extractMail(from), "some other string");
        Assert.assertNotEquals(Utils.extractMail(from), title);
        Assert.assertEquals(Utils.extractMail(null), "");
    }

    @Test
    public void parseDate() throws Exception {
        Assert.assertEquals(Utils.parseDate(dateRaw),dateFormatted);
        Assert.assertNotEquals(Utils.parseDate(dateRaw),"Fri, 23 Mar 2018 14:31:52");
        Assert.assertNotEquals(Utils.parseDate(dateRaw),"14:31:52");
    }

    @Test
    public void formatExpirationTime() throws Exception {
        Assert.assertEquals(Utils.formatExpirationTime(1521816593),"43:36");
        Assert.assertEquals(Utils.formatExpirationTime(0000000000),"0:00");
    }

    @Test
    public void formatLogTime() throws Exception {
        Assert.assertEquals(Utils.formatLogTime(1521816817),"16:43:36");
        Assert.assertNotEquals(Utils.formatLogTime(1521816817),"16:40:00");
    }

    @Test
    public void isEmpty() throws Exception {
        Assert.assertTrue(Utils.isEmpty(null));
        Assert.assertTrue(Utils.isEmpty(""));
        Assert.assertFalse(Utils.isEmpty("fdfd"));
    }
}