package com.example.masuikvova.tempmail.ui.MailDetails;

import com.example.masuikvova.tempmail.ui.Base.BaseView;

/**
 * Created by Vova on 12.03.2018.
 */

interface MailDetailsView extends BaseView {
    void setMailText(String text);

    void showLoginScreen();

    void showMailList();
}

