package com.example.masuikvova.tempmail.ui.Base;

import com.example.masuikvova.tempmail.helper.IScheduler;
import com.example.masuikvova.tempmail.repository.MailRepository;

/**
 * Created by Vova on 26.02.2018.
 */

public abstract class BasePresenter {
    protected MailRepository repository;
    protected IScheduler scheduler;

    public BasePresenter(MailRepository repository, IScheduler scheduler) {
        this.repository = repository;
        this.scheduler = scheduler;
    }

    public abstract void onViewAttached(BaseView baseView);

    public abstract void onViewDetached();
}
