package com.example.masuikvova.tempmail.ui.MailDetails;

import com.example.masuikvova.tempmail.TestUtils.MockScheduler;
import com.example.masuikvova.tempmail.TestUtils.TestRepository;
import com.example.masuikvova.tempmail.helper.IScheduler;
import com.example.masuikvova.tempmail.repository.MailRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class MailDetailsPresenterTest {

    private MailDetailsView view;
    private IScheduler scheduler;

    @Before
    public void setUp() {
        view = Mockito.mock(MailDetailsView.class);
        scheduler = new MockScheduler();
    }

    @Test
    public void successGetMailDetails() {
        MailRepository repository = new TestRepository(true, false);
        MailDetailsPresenter presenter = new MailDetailsPresenter(repository, scheduler);

        presenter.onViewAttached(view);
        presenter.getEmailDetails(0);

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).setMailText(Mockito.anyString());
    }

    @Test
    public void failGetMailDetails(){
        MailRepository repository = new TestRepository(false, false);
        MailDetailsPresenter presenter = new MailDetailsPresenter(repository, scheduler);

        presenter.onViewAttached(view);
        presenter.getEmailDetails(0);

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).showError(Mockito.anyString());
    }

    @Test
    public void invalidMailKey(){
        MailRepository repository = new TestRepository(false, true);
        MailDetailsPresenter presenter = new MailDetailsPresenter(repository, scheduler);

        presenter.onViewAttached(view);
        presenter.getEmailDetails(0);

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).showError(Mockito.anyString());
        Mockito.verify(view).showLoginScreen();
    }
}