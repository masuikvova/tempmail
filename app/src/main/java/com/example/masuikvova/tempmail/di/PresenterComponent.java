package com.example.masuikvova.tempmail.di;

import com.example.masuikvova.tempmail.ui.Login.LoginFragment;
import com.example.masuikvova.tempmail.ui.MailDetails.MailDetailsFragment;
import com.example.masuikvova.tempmail.ui.MailList.MailListFragment;
import com.example.masuikvova.tempmail.ui.MainActivity;

import dagger.Component;

/**
 * Created by Vova on 27.02.2018.
 */
@FragmentScope
@Component(dependencies = AppComponent.class, modules = {PresenterModule.class})
public interface PresenterComponent {

    void inject(LoginFragment loginFragment);

    void inject(MailListFragment mailListFragment);

    void inject(MailDetailsFragment mailDetails);

    void inject(MainActivity activity);
}
