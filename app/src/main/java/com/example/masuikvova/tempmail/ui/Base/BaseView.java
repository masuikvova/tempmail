package com.example.masuikvova.tempmail.ui.Base;

/**
 * Created by Vova on 26.02.2018.
 */

public interface BaseView {
    void showError(String msg);
    void showLoading(boolean show);
}
