package com.example.masuikvova.tempmail.ui.MailList;

import com.example.masuikvova.tempmail.TestUtils.MockScheduler;
import com.example.masuikvova.tempmail.TestUtils.TestRepository;
import com.example.masuikvova.tempmail.helper.IScheduler;
import com.example.masuikvova.tempmail.helper.Utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class MailListPresenterTest {

    private IScheduler scheduler = new MockScheduler();
    private MailListView view;


    @Before
    public void setUp() {
        view = Mockito.mock(MailListView.class);
    }


    @Test
    public void successGetMailList() {
        MailListPresenter presenter = new MailListPresenter(new TestRepository(true, false), scheduler);
        presenter.onViewAttached(view);
        presenter.getMailList();

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).showEmptyView(false);
        Mockito.verify(view).showMailList(Mockito.anyList());
    }

    @Test
    public void failGetMailList() {
        MailListPresenter presenter = new MailListPresenter(new TestRepository(false, false), scheduler);
        presenter.onViewAttached(view);
        presenter.getMailList();

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).showError(Mockito.anyString());
    }

    @Test
    public void failGetMailListKeyNotValid() {
        MailListPresenter presenter = new MailListPresenter(new TestRepository(false, true), scheduler);
        presenter.onViewAttached(view);
        presenter.getMailList();

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).showMessage(Mockito.anyString());
        Mockito.verify(view).showLoginScreen();
    }

    @Test
    public void successInitTimer(){
        MailListPresenter presenter = new MailListPresenter(new TestRepository(false, true), scheduler);
        presenter.onViewAttached(view);
        presenter.onResume();

        Mockito.verify(view).initTimer(Mockito.anyLong());
    }

    @Test
    public void successGetMailName(){
        MailListPresenter presenter = new MailListPresenter(new TestRepository(true, true), scheduler);
        presenter.onViewAttached(view);

        Assert.assertTrue(!Utils.isEmpty(presenter.getEmailAddress()));
    }

    @Test
    public void failGetMailName(){
        MailListPresenter presenter = new MailListPresenter(new TestRepository(false, true), scheduler);
        presenter.onViewAttached(view);

        Assert.assertTrue(Utils.isEmpty(presenter.getEmailAddress()));
    }


    //test updating time
    @Test
    public void successUpdateTime(){
        MailListPresenter presenter = new MailListPresenter(new TestRepository(true, true), scheduler);
        presenter.onViewAttached(view);
        presenter.updateExpirationTime();

        Mockito.verify(view).initTimer(Mockito.anyLong());
        Mockito.verify(view).showMessage(Mockito.anyString());
    }

    @Test
    public void failUpdateTime(){
        MailListPresenter presenter = new MailListPresenter(new TestRepository(false, false), scheduler);
        presenter.onViewAttached(view);
        presenter.updateExpirationTime();

        Mockito.verify(view).showError(Mockito.anyString());
    }

    @Test
    public void failUpdateTimeKeyNotValid(){
        MailListPresenter presenter = new MailListPresenter(new TestRepository(false, true), scheduler);
        presenter.onViewAttached(view);
        presenter.updateExpirationTime();

        Mockito.verify(view).showMessage(Mockito.anyString());
        Mockito.verify(view).showLoginScreen();
    }

    // test clear list
    @Test
    public void successClearList(){
        MailListPresenter presenter = new MailListPresenter(new TestRepository(true, true), scheduler);
        presenter.onViewAttached(view);
        presenter.clearMailbox();

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).showMailList(Mockito.anyList());
        Mockito.verify(view).showEmptyView(true);
    }

    @Test
    public void failClearList(){
        MailListPresenter presenter = new MailListPresenter(new TestRepository(false, false), scheduler);
        presenter.onViewAttached(view);
        presenter.clearMailbox();

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).showError(Mockito.anyString());
    }

    @Test
    public void failClearListKeyNotValid(){
        MailListPresenter presenter = new MailListPresenter(new TestRepository(false, true), scheduler);
        presenter.onViewAttached(view);
        presenter.clearMailbox();

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).showMessage(Mockito.anyString());
        Mockito.verify(view).showLoginScreen();
    }

    // test delete mail box
    @Test
    public void successDeleteBox(){
        MailListPresenter presenter = new MailListPresenter(new TestRepository(true, true), scheduler);
        presenter.onViewAttached(view);
        presenter.deleteMailBox();

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).showLoginScreen();
    }

    @Test
    public void failDeleteBox(){
        MailListPresenter presenter = new MailListPresenter(new TestRepository(false, false), scheduler);
        presenter.onViewAttached(view);
        presenter.deleteMailBox();

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).showError(Mockito.anyString());
    }

    @Test
    public void failDeleteBoxKeyNotValid(){
        MailListPresenter presenter = new MailListPresenter(new TestRepository(false, true), scheduler);
        presenter.onViewAttached(view);
        presenter.deleteMailBox();

        Mockito.verify(view).showLoading(true);
        Mockito.verify(view).showLoading(false);
        Mockito.verify(view).showMessage(Mockito.anyString());
        Mockito.verify(view).showLoginScreen();
    }
}