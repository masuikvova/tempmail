package com.example.masuikvova.tempmail.repository.LocalData;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.masuikvova.tempmail.models.Account;

public class LocalDataSourceImp implements LocalDataSource {
    private static final String ACCOUNT_EMAIL = "user_email";
    private static final String ACCOUNT_KEY = "user_key";
    private static final String EXPIRATION_TIME = "expiration_time";

    private static final long MINUTES_10 = 600000;
    private static final long HOUR = 3600000;

    private SharedPreferences prefs;

    public LocalDataSourceImp(Context ctx) {
        Context context = ctx.getApplicationContext();
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public Account getAccount() {
        return new Account(prefs.getString(ACCOUNT_KEY, ""),
                prefs.getString(ACCOUNT_EMAIL, ""));
    }

    @Override
    public void saveAccount(Account account, boolean withName) {
        prefs.edit().clear().apply();
        prefs.edit().putString(ACCOUNT_KEY, account.getKey()).apply();
        prefs.edit().putString(ACCOUNT_EMAIL, account.getEmail()).apply();
        prefs.edit().putLong(EXPIRATION_TIME, withName ? System.currentTimeMillis() + HOUR : System.currentTimeMillis() + MINUTES_10).apply();
    }

    @Override
    public void saveExpirationTime(long time) {
        prefs.edit().putLong(EXPIRATION_TIME, time).apply();
    }

    @Override
    public long getExpirationTime() {
        return prefs.getLong(EXPIRATION_TIME, 0);
    }

    @Override
    public void clearUserData() {
        prefs.edit().clear().apply();
    }
}
