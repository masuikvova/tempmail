package com.example.masuikvova.tempmail.ui.MailList;


import com.example.masuikvova.tempmail.helper.IScheduler;
import com.example.masuikvova.tempmail.models.BaseModel;
import com.example.masuikvova.tempmail.models.Email;
import com.example.masuikvova.tempmail.repository.MailRepository;
import com.example.masuikvova.tempmail.ui.Base.BasePresenter;
import com.example.masuikvova.tempmail.ui.Base.BaseView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vova on 01.03.2018.
 */

public class MailListPresenter extends BasePresenter {
    private MailListView view;
    private String key;

    public MailListPresenter(MailRepository repository, IScheduler scheduler) {
        super(repository, scheduler);
        key = repository.getAccount().getKey();
    }

    @Override
    public void onViewAttached(BaseView baseView) {
        this.view = (MailListView) baseView;
    }

    @Override
    public void onViewDetached() {
        this.view = null;
    }

    public void onResume() {
        view.initTimer(repository.getExpirationTime() - System.currentTimeMillis());
    }

    public void getMailList() {
        repository.getEmailList(key)
                .compose(scheduler.getThreadTransformer())
                .doOnSubscribe(() -> view.showLoading(true))
                .doAfterTerminate(() -> view.showLoading(false))
                .subscribe(this::handleResponse, this::handleError);
    }

    public String getEmailAddress() {
        return repository.getAccount().getEmail();
    }

    public void updateExpirationTime() {
        repository.updateTime(key)
                .compose(scheduler.getThreadTransformer())
                .subscribe(status -> {
                    if (!status.hasError()) {
                        onResume();
                        view.showMessage("Time updated");
                    } else
                        view.showError(status.getErrorMsg());
                }, this::handleError);
    }

    public void clearMailbox() {
        repository.clearMailBox(key)
                .compose(scheduler.getThreadTransformer())
                .doOnSubscribe(() -> view.showLoading(true))
                .doAfterTerminate(() -> view.showLoading(false))
                .subscribe(status -> {
                    if (status.isClear()) {
                        view.showMailList(new ArrayList<>());
                        view.showEmptyView(true);
                    }
                    if (status.hasError())
                        view.showError(status.getErrorMsg());

                }, this::handleError);
    }

    public void deleteMailBox() {
        repository.deleteMailBox(key)
                .compose(scheduler.getThreadTransformer())
                .doOnSubscribe(() -> view.showLoading(true))
                .doAfterTerminate(() -> view.showLoading(false))
                .subscribe(status -> {
                    if (status.isDeleted())
                        view.showLoginScreen();
                    if (status.hasError())
                        view.showError(status.getErrorMsg());
                }, this::handleError);
    }

    private void handleResponse(List<Email> data) {
        if (data != null) {
            view.showEmptyView(data.isEmpty());
            view.showMailList(data);
        }
    }

    private void handleError(Throwable throwable) {
        if (throwable.getMessage().equals(BaseModel.KEY_NOT_FOUND)) {
            view.showMessage("Email box no more valid");
            view.showLoginScreen();
        } else
            view.showError(throwable.getMessage());
    }
}
