package com.example.masuikvova.tempmail.ui.Navigation;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;

import com.example.masuikvova.tempmail.R;
import com.example.masuikvova.tempmail.models.Email;
import com.example.masuikvova.tempmail.ui.Login.LoginFragment;
import com.example.masuikvova.tempmail.ui.MailDetails.MailDetailsFragment;
import com.example.masuikvova.tempmail.ui.MailList.MailListFragment;

/**
 * Created by Vova on 01.03.2018.
 */

public class RouterImp implements Router {
    private FragmentManager fragmentManager;

    public RouterImp(AppCompatActivity activity) {
        fragmentManager = activity.getSupportFragmentManager();
    }

    @Override
    public void showLoginScreen() {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, new LoginFragment());
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void showMailList() {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, new MailListFragment());
        fragmentTransaction.addToBackStack(MailListFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void showMailDetails(Email email) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = MailDetailsFragment.newInstance(email);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            fragment.setEnterTransition(new Fade(Fade.IN).setDuration(300));
            fragment.setReturnTransition(new Fade(Fade.OUT).setDuration(300));
            fragmentTransaction.add(R.id.fragmentContainer, fragment);
        } else {
            fragmentTransaction.add(R.id.fragmentContainer, fragment);
        }
        fragmentTransaction.addToBackStack(MailDetailsFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void backScreen() {
        fragmentManager.popBackStack();
    }
}
