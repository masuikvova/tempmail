package com.example.masuikvova.tempmail.ui.MailList;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.masuikvova.tempmail.MailApplication;
import com.example.masuikvova.tempmail.R;
import com.example.masuikvova.tempmail.di.DaggerPresenterComponent;
import com.example.masuikvova.tempmail.di.PresenterComponent;
import com.example.masuikvova.tempmail.helper.Utils;
import com.example.masuikvova.tempmail.models.Email;
import com.example.masuikvova.tempmail.ui.Base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Vova on 01.03.2018.
 */

public class MailListFragment extends BaseFragment implements MailListView {
    @BindView(R.id.empty)
    View emptyView;
    @BindView(R.id.mailList)
    RecyclerView mailList;
    @BindView(R.id.dlMenu)
    DrawerLayout dlMenu;
    @BindView(R.id.left_drawer)
    RelativeLayout leftMenu;
    @BindView(R.id.tvTimeLeft)
    TextView timerLabel;
    @BindView(R.id.tvMenuMail)
    TextView mailLabel;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.emptyImg)
    ImageView emptyImg;
    @Inject
    MailListPresenter presenter;
    private Unbinder unbinder;
    private MailListAdapter adapter;
    private CountDownTimer timer;
    private Animation scaleAnim;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_mail_list, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        initPresenter();
        initView();
        presenter.onViewAttached(this);
        presenter.getMailList();
        return rootView;
    }

    private void initPresenter() {
        PresenterComponent presenterComponent = DaggerPresenterComponent.builder().appComponent(MailApplication.get(getContext()).getAppComponent()).build();
        presenterComponent.inject(this);
    }

    private void initView() {
        scaleAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_scale);
        scaleAnim.setInterpolator(time -> (float) (-1 * Math.pow(Math.E, -time / 0.4) * Math.cos(20 * time) + 1));
        mailLabel.setText(presenter.getEmailAddress());
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        refreshLayout.setOnRefreshListener(() -> presenter.getMailList());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mailList.setLayoutManager(layoutManager);
        mailList.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.HORIZONTAL));
        adapter = new MailListAdapter(getRouter());
        mailList.setAdapter(adapter);
    }

    public void initTimer(long time) {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        timer = new CountDownTimer(time, 1000) {
            public void onTick(long millisUntilFinished) {
                if (isAdded() && dlMenu.isDrawerOpen(leftMenu))
                    timerLabel.setText(String.format(getString(R.string.time_left), Utils.formatExpirationTime(millisUntilFinished)));
            }

            public void onFinish() {
                if (isAdded() && dlMenu.isDrawerOpen(leftMenu))
                    timerLabel.setText(String.format(getString(R.string.time_left), "0:00"));
            }
        }.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onViewDetached();
        unbinder.unbind();
    }

    @Override
    public void showLoading(boolean show) {
        if (isAdded()) {
            refreshLayout.setRefreshing(show);
        }
    }

    @Override
    public void showMessage(String msg) {
        if (isAdded() && getActivity() != null && msg != null)
            Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMailList(List<Email> data) {
        adapter.setData(data);
    }

    @Override
    public void showEmptyView(boolean show) {
        if (isAdded()) {
            emptyView.setVisibility(show ? View.VISIBLE : View.GONE);
            emptyImg.startAnimation(scaleAnim);
        }
    }

    @Override
    public void showLoginScreen() {
        getRouter().showLoginScreen();
    }

    @OnClick({R.id.rlItemACopy, R.id.rlItemUpdate, R.id.rlItemClear, R.id.rlItemDelete})
    void menuHandler(View v) {
        switch (v.getId()) {
            case R.id.rlItemACopy:
                String email = presenter.getEmailAddress();
                copyEmail(email);
                showMessage(getString(R.string.msg_email_copied));
                break;
            case R.id.rlItemUpdate:
                presenter.updateExpirationTime();
                break;
            case R.id.rlItemClear:
                presenter.clearMailbox();
                break;
            case R.id.rlItemDelete:
                presenter.deleteMailBox();
                break;
        }
        dlMenu.closeDrawer(leftMenu);
    }

    @OnClick(R.id.home)
    void homeAction() {
        dlMenu.openDrawer(leftMenu);
    }

    private void copyEmail(String email) {
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("email", email);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
    }
}
