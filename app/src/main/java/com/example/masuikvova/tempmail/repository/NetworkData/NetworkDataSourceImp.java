package com.example.masuikvova.tempmail.repository.NetworkData;

import com.example.masuikvova.tempmail.models.Account;
import com.example.masuikvova.tempmail.models.BaseModel;
import com.example.masuikvova.tempmail.models.Content;
import com.example.masuikvova.tempmail.models.Email;
import com.example.masuikvova.tempmail.models.Status;
import com.example.masuikvova.tempmail.repository.api.MailService;

import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Vova on 21.02.2018.
 */

public class NetworkDataSourceImp implements NetworkDataSource {

    private final MailService service;

    private Func1<Status, Observable<Status>> keyHandler = status -> Observable.create((Observable.OnSubscribe<Status>) subscriber -> {
        if (status.isKeyNotFound())
            subscriber.onError(new Throwable(BaseModel.KEY_NOT_FOUND));
        else if (status.hasError())
            subscriber.onError(new Throwable(status.getErrorMsg()));
        else {
            subscriber.onNext(status);
            subscriber.onCompleted();
        }
    });

    public NetworkDataSourceImp(MailService service) {
        this.service = service;
    }

    public Observable<Account> createMainBox() {
        return service.createEmail();
    }

    public Observable<Account> createMainBox(String mailBoxName) {
        return service.createEmailByName(mailBoxName);
    }

    public Observable<Status> clearMailBox(String key) {
        return service.clearMailBox(key).flatMap(keyHandler);
    }

    public Observable<Status> deleteMailBox(String key) {
        return service.deleteMailBox(key).flatMap(keyHandler);
    }

    public Observable<Status> getExpirationTime(String key) {
        return service.getExpirationTime(key).flatMap(keyHandler);
    }

    public Observable<Status> updateTime(String key) {
        return service.updateTime(key).flatMap(keyHandler);
    }

    public Observable<List<Email>> getEmailList(String key) {
        return service.getMailList(key)
                .flatMap(emailList ->
                        Observable.create((Observable.OnSubscribe<List<Email>>) subscriber -> {
                            if (emailList.isKeyNotFound())
                                subscriber.onError(new Throwable(BaseModel.KEY_NOT_FOUND));
                            else if (emailList.hasError())
                                subscriber.onError(new Throwable(emailList.getErrorMsg()));
                            else {
                                subscriber.onNext(emailList.getEmails());
                                subscriber.onCompleted();
                            }
                        })
                );
    }

    public Observable<Content> getMailContent(String key, int id) {
        return service.getMailText(key, id).flatMap(content -> Observable.create((Observable.OnSubscribe<Content>) subscriber -> {
            if (content.isKeyNotFound())
                subscriber.onError(new Throwable(BaseModel.KEY_NOT_FOUND));
            else if (content.hasError())
                subscriber.onError(new Throwable(content.getErrorMsg()));
            else {
                subscriber.onNext(content);
                subscriber.onCompleted();
            }
        }));
    }
}
