package com.example.masuikvova.tempmail.repository.api;

import com.example.masuikvova.tempmail.models.Account;
import com.example.masuikvova.tempmail.models.Content;
import com.example.masuikvova.tempmail.models.EmailList;
import com.example.masuikvova.tempmail.models.Status;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Vova on 21.02.2018.
 */

public interface MailService {

    @GET("api.php?action=new&type=json")
    Observable<Account> createEmail();

    @GET("api.php?action=new&type=json")
    Observable<Account> createEmailByName(@Query("name") String emailName);

    @GET("api.php?action=getmail&type=json")
    Observable<Content> getMailText(@Query("key") String key, @Query("id") int id);

    @GET("api.php?action=livetime&type=json")
    Observable<Status> getExpirationTime(@Query("key") String key);

    @GET("api.php?action=update&type=json")
    Observable<Status> updateTime(@Query("key") String key);

    @GET("api.php?action=delete&type=json")
    Observable<Status> deleteMailBox(@Query("key") String key);

    @GET("api.php?action=clear&type=json")
    Observable<Status> clearMailBox(@Query("key") String key);

    @GET("api.php?action=getlist&type=json")
    Observable<EmailList> getMailList(@Query("key") String key);
}
