package com.example.masuikvova.tempmail.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vova on 21.02.2018.
 */

public class BaseModel {
    public static final String KEY_NOT_FOUND = "key_not_found";
    public static final String OK = "ok";

    @SerializedName("error")
    private String errorMsg = null;

    public String getErrorMsg() {
        return errorMsg;
    }

    public boolean hasError() {
        return errorMsg != null;
    }

    public boolean isKeyNotFound() {
        return errorMsg != null && errorMsg.equals(KEY_NOT_FOUND);
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
