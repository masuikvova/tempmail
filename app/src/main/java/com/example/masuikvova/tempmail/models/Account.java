package com.example.masuikvova.tempmail.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vova on 21.02.2018.
 */

public class Account extends BaseModel{

    @SerializedName("email")
    private String email;

    @SerializedName("key")
    private String key;

    public Account(String key, String email) {
        this.email = email;
        this.key = key;
    }

    public String getEmail() {
        return email;
    }

    public String getKey() {
        return key;
    }

    public boolean isEmpty() {
        return email.equals("") || key.equals("");
    }

    @Override
    public String toString() {
        return "Account{" +
                "email='" + email + '\'' +
                ", key='" + key + '\'' +
                '}';
    }
}
