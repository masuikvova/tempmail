package com.example.masuikvova.tempmail.ui.Base;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;

import com.example.masuikvova.tempmail.ui.MainActivity;
import com.example.masuikvova.tempmail.ui.Navigation.Router;

/**
 * Created by Vova on 26.02.2018.
 */

public class BaseFragment extends Fragment implements BaseView {

    public Router getRouter() {
        if (getActivity() != null)
            return ((MainActivity) getActivity()).getRouter();
        else
            return null;
    }

    @Override
    public void showError(String msg) {
        if (isAdded() && getView() != null)
            Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLoading(boolean show) {

    }
}
