package com.example.masuikvova.tempmail.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vova on 21.02.2018.
 */

public class Status extends BaseModel {

    @SerializedName("key")
    private String key;

    @SerializedName("clear")
    private String clear;

    @SerializedName("delete")
    private String delete;

    @SerializedName("livetime")
    private String liveTime;

    public Status() {
    }

    public Status(String clear, String delete, String liveTime) {
        this.clear = clear;
        this.delete = delete;
        this.liveTime = liveTime;
    }

    public int getLiveTime() {
        return Integer.parseInt(liveTime == null ? "0" : liveTime);
    }

    public boolean isDeleted() {
        return delete != null && delete.equals(OK);
    }

    public boolean isClear() {
        return clear != null && clear.equals(OK);
    }
}

