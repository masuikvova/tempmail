package com.example.masuikvova.tempmail.ui.MailDetails;

import com.example.masuikvova.tempmail.helper.IScheduler;
import com.example.masuikvova.tempmail.models.BaseModel;
import com.example.masuikvova.tempmail.models.Content;
import com.example.masuikvova.tempmail.repository.MailRepository;
import com.example.masuikvova.tempmail.ui.Base.BasePresenter;
import com.example.masuikvova.tempmail.ui.Base.BaseView;

/**
 * Created by Vova on 12.03.2018.
 */

public class MailDetailsPresenter extends BasePresenter {
    private MailDetailsView view;

    public MailDetailsPresenter(MailRepository repository, IScheduler scheduler) {
        super(repository, scheduler);
    }

    @Override
    public void onViewAttached(BaseView baseView) {
        view = (MailDetailsView) baseView;
    }

    @Override
    public void onViewDetached() {
        view = null;
    }

    public void getEmailDetails(int id) {
        repository.getMailContent(repository.getAccount().getKey(), id)
                .compose(scheduler.getThreadTransformer())
                .doOnSubscribe(() -> view.showLoading(true))
                .doAfterTerminate(() -> view.showLoading(false))
                .subscribe(this::handleResponse, this::handleError);
    }

    private void handleResponse(Content content) {
        if (content.getContent() == null)
            view.showError("Can`t get mail content. Server side error");
        else
            view.setMailText(content.getContent());
    }

    private void handleError(Throwable throwable) {
        if (throwable.getMessage().equals(BaseModel.KEY_NOT_FOUND)) {
            view.showError("Email box no more valid");
            view.showLoginScreen();
        } else
            view.showError(throwable.getMessage());
    }
}
