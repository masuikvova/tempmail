package com.example.masuikvova.tempmail.ui.MailList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.masuikvova.tempmail.R;
import com.example.masuikvova.tempmail.helper.Utils;
import com.example.masuikvova.tempmail.models.Email;
import com.example.masuikvova.tempmail.ui.Navigation.Router;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Vova on 06.03.2018.
 */

public class MailListViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvTitle)
    TextView title;
    @BindView(R.id.tvSubject)
    TextView subject;
    @BindView(R.id.tvSign)
    TextView sign;
    @BindView(R.id.tvDate)
    TextView date;
    @BindView(R.id.tvEmail)
    TextView mailAddress;
    private Router router;
    private Email email;

    public static RecyclerView.ViewHolder create(ViewGroup parent, Router router) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mail_list, parent, false);
        return new MailListViewHolder(view, router);
    }

    public MailListViewHolder(View itemView, Router router) {
        super(itemView);
        this.router = router;
        ButterKnife.bind(this, itemView);
    }

    public void init(Email email) {
        if (email == null)
            return;
        this.email = email;
        title.setText(Utils.formatTitle(email.getFrom()));
        mailAddress.setText(Utils.extractMail(email.getFrom()));
        date.setText(Utils.parseDate(email.getData()));
        subject.setText(email.getSubject());
        sign.setText(Utils.getSign(email.getFrom()));
    }

    @OnClick(R.id.item_parent)
    void showDetails() {
        router.showMailDetails(email);
    }
}
