package com.example.masuikvova.tempmail.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vova on 21.02.2018.
 */

public class Content extends BaseModel {
    @SerializedName("message")
    private String content;

    public Content(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
