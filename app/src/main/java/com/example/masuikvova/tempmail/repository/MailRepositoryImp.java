package com.example.masuikvova.tempmail.repository;

import com.example.masuikvova.tempmail.models.Account;
import com.example.masuikvova.tempmail.models.Content;
import com.example.masuikvova.tempmail.models.Email;
import com.example.masuikvova.tempmail.models.Status;
import com.example.masuikvova.tempmail.repository.LocalData.LocalDataSource;
import com.example.masuikvova.tempmail.repository.NetworkData.NetworkDataSource;

import java.util.List;

import rx.Observable;

/**
 * Created by Vova on 22.02.2018.
 */

public class MailRepositoryImp implements MailRepository {

    private LocalDataSource localDataSource;
    private NetworkDataSource networkDataSource;

    public MailRepositoryImp(LocalDataSource localDataSource, NetworkDataSource networkDataSource) {
        this.localDataSource = localDataSource;
        this.networkDataSource = networkDataSource;
    }

    @Override
    public Account getAccount() {
        return localDataSource.getAccount();
    }

    @Override
    public long getExpirationTime() {
        return localDataSource.getExpirationTime();
    }

    @Override
    public Observable<Account> createMainBox() {
        return networkDataSource.createMainBox()
                .flatMap(account -> {
                    if (!account.hasError())
                        localDataSource.saveAccount(account, false);
                    return Observable.just(account);
                });
    }

    @Override
    public Observable<Account> createMainBox(String mailBoxName) {
        return networkDataSource.createMainBox(mailBoxName)
                .flatMap(account -> {
                    if (!account.hasError())
                        localDataSource.saveAccount(account, true);
                    return Observable.just(account);
                });
    }

    @Override
    public Observable<Status> clearMailBox(String key) {
        return networkDataSource.clearMailBox(key);
    }

    @Override
    public Observable<Status> deleteMailBox(String key) {
        return networkDataSource.deleteMailBox(key)
                .flatMap(status -> {
                    if (!status.hasError())
                        localDataSource.clearUserData();
                    return Observable.just(status);
                });
    }

    @Override
    public Observable<Status> getExpirationTime(String key) {
        return networkDataSource.getExpirationTime(key)
                .flatMap(status -> {
                    if (!status.hasError())
                        localDataSource.saveExpirationTime(System.currentTimeMillis() + status.getLiveTime()*1000);
                    if(status.isKeyNotFound())
                        localDataSource.clearUserData();
                    return Observable.just(status);
                });
    }

    @Override
    public Observable<Status> updateTime(String key) {
        return networkDataSource.updateTime(key)
                .flatMap(status -> {
                    if (!status.hasError())
                        localDataSource.saveExpirationTime(System.currentTimeMillis() + status.getLiveTime()*1000);
                    return Observable.just(status);
                });
    }

    @Override
    public Observable<List<Email>> getEmailList(String key) {
        return networkDataSource.getEmailList(key);
    }

    @Override
    public Observable<Content> getMailContent(String key, int id) {
        return networkDataSource.getMailContent(key, id);
    }
}
