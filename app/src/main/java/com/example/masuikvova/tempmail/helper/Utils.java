package com.example.masuikvova.tempmail.helper;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.example.masuikvova.tempmail.BuildConfig;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Vova on 06.03.2018.
 */

public class Utils {
    private static final String TAG = "TempMail";

    public static void log(String msg) {
        if (BuildConfig.DEBUG)
            Log.v(TAG, msg);
    }

    public static void hideKeyboard(Activity activity) {
        if (activity == null) return;
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @NonNull
    public static String getSign(String s) {
        if (s != null && s.length() > 0)
            return String.valueOf(s.charAt(0)).toUpperCase();
        else
            return "";
    }

    public static String formatTitle(String s) {
        if (s != null && s.length() > 0) {
            int ind = s.indexOf("<");
            if (ind > 0)
                return s.substring(0, ind);
        }
        return s;
    }

    @NonNull
    public static String extractMail(String s) {
        if (s != null) {
            int start = s.indexOf("<");
            int end = s.indexOf(">");
            if (start > 0 && end > 0)
                return s.substring(start + 1, end);
        }
        return "";
    }

    public static String parseDate(String inData) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.US);
        SimpleDateFormat outFormat = new SimpleDateFormat("dd:MM:yy HH:mm", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(inData);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outFormat.format(date);
    }

    public static String formatExpirationTime(long time) {
        return new SimpleDateFormat("m:ss", Locale.US).format(time);
    }

    public static String formatLogTime(long time) {
        return new SimpleDateFormat("HH:mm:ss", Locale.US).format(time);
    }

    public static boolean isEmpty(@Nullable CharSequence text) {
        return text == null || text.length() == 0;
    }
}
