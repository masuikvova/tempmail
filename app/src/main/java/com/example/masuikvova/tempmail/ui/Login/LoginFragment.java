package com.example.masuikvova.tempmail.ui.Login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.masuikvova.tempmail.MailApplication;
import com.example.masuikvova.tempmail.R;
import com.example.masuikvova.tempmail.di.DaggerPresenterComponent;
import com.example.masuikvova.tempmail.di.PresenterComponent;
import com.example.masuikvova.tempmail.helper.Utils;
import com.example.masuikvova.tempmail.ui.Base.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Vova on 26.02.2018.
 */

public class LoginFragment extends BaseFragment implements LoginView {
    @BindView(R.id.loginCard)
    CardView loginCard;
    @BindView(R.id.etName)
    EditText emailInput;
    @BindView(R.id.loading)
    View loading;
    @BindView(R.id.help)
    View help;
    @Inject
    LoginPresenter presenter;
    private Unbinder unbinder;
    private BottomSheetBehavior sheetBehavior;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        initPresenter();
        initView();
        presenter.onViewAttached(this);
        return rootView;
    }

    private void initView() {
        sheetBehavior = BottomSheetBehavior.from(help);
    }

    private void initPresenter() {
        PresenterComponent presenterComponent = DaggerPresenterComponent.builder().appComponent(MailApplication.get(getActivity()).getAppComponent()).build();
        presenterComponent.inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onViewDetached();
        unbinder.unbind();
    }

    @Override
    public void showLoading(boolean show) {
        if (isAdded()) {
            loading.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onCreationSuccess() {
        getRouter().showMailList();
    }

    @OnClick(R.id.btnCreateMail)
    void createEmailBox() {
        Utils.hideKeyboard(getActivity());
        presenter.createEmailBox(emailInput.getText().toString());
    }

    @OnClick(R.id.ivHelp)
    void showHelp() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED)
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        else
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @OnClick(R.id.closeHelp)
    void closeHelp(){
        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }
}
