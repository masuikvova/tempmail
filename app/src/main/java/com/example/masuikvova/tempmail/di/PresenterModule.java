package com.example.masuikvova.tempmail.di;

import com.example.masuikvova.tempmail.helper.IScheduler;
import com.example.masuikvova.tempmail.repository.MailRepository;
import com.example.masuikvova.tempmail.ui.ActivityPresenter;
import com.example.masuikvova.tempmail.ui.Login.LoginPresenter;
import com.example.masuikvova.tempmail.ui.MailDetails.MailDetailsPresenter;
import com.example.masuikvova.tempmail.ui.MailList.MailListPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Vova on 27.02.2018.
 */
@Module
public class PresenterModule {
    @Provides
    @FragmentScope
    public LoginPresenter provideLoginPresenter(MailRepository repository, IScheduler scheduler) {
        return new LoginPresenter(repository, scheduler);
    }

    @Provides
    @FragmentScope
    public MailListPresenter provideMailListPresenter(MailRepository repository, IScheduler scheduler) {
        return new MailListPresenter(repository, scheduler);
    }

    @Provides
    @FragmentScope
    public MailDetailsPresenter provideMailDetailsPresenterr(MailRepository repository, IScheduler scheduler) {
        return new MailDetailsPresenter(repository, scheduler);
    }

    @Provides
    @FragmentScope
    public ActivityPresenter provideActivityPresenter(MailRepository repository, IScheduler scheduler) {
        return new ActivityPresenter(repository, scheduler);
    }
}
