package com.example.masuikvova.tempmail.TestUtils;

import com.example.masuikvova.tempmail.helper.IScheduler;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by Vova on 27.03.18.
 */

public class MockScheduler implements IScheduler {
    @Override
    public <T> Observable.Transformer<T, T> getThreadTransformer() {
        return observable -> observable
                .subscribeOn(Schedulers.immediate())
                .observeOn(Schedulers.immediate());
    }
}
