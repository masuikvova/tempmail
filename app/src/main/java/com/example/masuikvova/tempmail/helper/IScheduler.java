package com.example.masuikvova.tempmail.helper;

import rx.Observable;

/**
 * Created by android on 23.03.18.
 */


public interface IScheduler {
    <T> Observable.Transformer<T, T> getThreadTransformer();
}
