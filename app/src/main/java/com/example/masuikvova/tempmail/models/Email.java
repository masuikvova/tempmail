package com.example.masuikvova.tempmail.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vova on 21.02.2018.
 */

public class Email implements Parcelable {
    @SerializedName("id")
    private int id;

    @SerializedName("date")
    private String data;

    @SerializedName("subject")
    private String subject;

    @SerializedName("from")
    private String from;

    public Email() {
    }

    public Email(int id, String data, String subject, String from) {
        this.id = id;
        this.data = data;
        this.subject = subject;
        this.from = from;
    }

    protected Email(Parcel in) {
        id = in.readInt();
        data = in.readString();
        subject = in.readString();
        from = in.readString();
    }

    public static final Parcelable.Creator<Email> CREATOR = new Creator<Email>() {
        @Override
        public Email createFromParcel(Parcel in) {
            return new Email(in);
        }

        @Override
        public Email[] newArray(int size) {
            return new Email[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public String getSubject() {
        return subject;
    }

    public String getFrom() {
        return from;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.data);
        parcel.writeString(this.from);
        parcel.writeString(this.subject);
    }

    @Override
    public boolean equals(Object obj) {
        return obj == this || obj != null && obj instanceof Email && this.id == ((Email) obj).getId();
    }

    @Override
    public int hashCode() {
        return id;
    }
}
