package com.example.masuikvova.tempmail.TestUtils;

import com.example.masuikvova.tempmail.models.Account;
import com.example.masuikvova.tempmail.models.BaseModel;
import com.example.masuikvova.tempmail.models.Content;
import com.example.masuikvova.tempmail.models.Email;
import com.example.masuikvova.tempmail.models.Status;
import com.example.masuikvova.tempmail.repository.MailRepository;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by Vova on 27.03.18.
 */

public class TestRepository implements MailRepository {
    public static final String TEST_ERROR = "some error";
    public static final String CONTENT = "Some content";
    private boolean isSuccess = false;
    private List<Email> emails = new ArrayList<>();
    private Status error;
    private Status success;
    private Account account;
    private boolean isKeyNotFound;

    public TestRepository(boolean isSuccess, boolean isKeyNotFount) {
        this.isSuccess = isSuccess;
        this.isKeyNotFound = isKeyNotFount;
        emails.add(new Email(23, "Thu, 09 Mar 2017 11:06:11 +0300", "Подтверждение регистрации", "noreply@icq.com"));
        emails.add(new Email(23, "Thu, 09 Mar 2017 11:06:11 +0300", "Подтверждение регистрации", "noreply@icq.com"));
        success = new Status(BaseModel.OK, BaseModel.OK, "600");
        error = new Status();
        error.setErrorMsg(TEST_ERROR);
        account = new Account("faf3f53538d0b5a52222ad7ef5040068", "bjlokwva72@post-shift.ru");
    }

    @Override
    public Account getAccount() {
        if (!isSuccess)
            return new Account("", "");
        return account;
    }

    public void setup(boolean isSuccess, boolean isKeyNotFount) {
        this.isSuccess = isSuccess;
        this.isKeyNotFound = isKeyNotFount;
    }

    @Override
    public long getExpirationTime() {
        return System.currentTimeMillis() + 10 * 1000;
    }

    @Override
    public Observable<Account> createMainBox() {
        return createAccount();
    }

    @Override
    public Observable<Account> createMainBox(String mailBoxName) {
        return createAccount();
    }

    @Override
    public Observable<Status> clearMailBox(String key) {
        emails.clear();
        return makeResponse();
    }

    @Override
    public Observable<Status> deleteMailBox(String key) {
        return makeResponse();
    }

    @Override
    public Observable<Status> getExpirationTime(String key) {
        return makeResponse();
    }

    @Override
    public Observable<Status> updateTime(String key) {
        return makeResponse();
    }

    @Override
    public Observable<List<Email>> getEmailList(String key) {
        if (isSuccess)
            return Observable.just(emails);
        else {
            if (isKeyNotFound)
                return Observable.error(new Throwable(BaseModel.KEY_NOT_FOUND));
            else
                return Observable.error(new Throwable(TEST_ERROR));
        }
    }

    @Override
    public Observable<Content> getMailContent(String key, int id) {
        if (isSuccess)
            return Observable.just(new Content(CONTENT));
        else {
            if (isKeyNotFound)
                return Observable.error(new Throwable(BaseModel.KEY_NOT_FOUND));
            else {
                Content content = new Content(null);
                content.setErrorMsg(TEST_ERROR);
                return Observable.just(content);
            }
        }
    }

    private Observable<Account> createAccount() {
        if (isSuccess)
            return Observable.just(account);
        else {
            return Observable.error(new Throwable(TEST_ERROR));
        }
    }

    private Observable<Status> makeResponse() {
        if (isSuccess)
            return Observable.just(success);
        else {
            if (isKeyNotFound)
                return Observable.error(new Throwable(BaseModel.KEY_NOT_FOUND));
            else
                return Observable.just(error);
        }
    }
}

