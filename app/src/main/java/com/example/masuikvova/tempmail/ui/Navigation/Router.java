package com.example.masuikvova.tempmail.ui.Navigation;

import com.example.masuikvova.tempmail.models.Email;

/**
 * Created by Vova on 01.03.2018.
 */

public interface Router {

    void showLoginScreen();

    void showMailList();

    void showMailDetails(Email email);

    void backScreen();
}
