package com.example.masuikvova.tempmail.di;

import android.app.Application;

import com.example.masuikvova.tempmail.helper.IScheduler;
import com.example.masuikvova.tempmail.helper.WorkScheduler;
import com.example.masuikvova.tempmail.repository.api.ApiFactory;
import com.example.masuikvova.tempmail.repository.api.MailService;
import com.example.masuikvova.tempmail.repository.LocalData.LocalDataSource;
import com.example.masuikvova.tempmail.repository.LocalData.LocalDataSourceImp;
import com.example.masuikvova.tempmail.repository.MailRepository;
import com.example.masuikvova.tempmail.repository.MailRepositoryImp;
import com.example.masuikvova.tempmail.repository.NetworkData.NetworkDataSource;
import com.example.masuikvova.tempmail.repository.NetworkData.NetworkDataSourceImp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Vova on 25.02.2018.
 */
@Module
public class MailModule {

    private Application application;

    public MailModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    public MailService provideMailService() {
        return ApiFactory.createService();
    }

    @Provides
    @Singleton
    public NetworkDataSource provideNetworkDataSource(MailService service) {
        return new NetworkDataSourceImp(service);
    }

    @Provides
    @Singleton
    public LocalDataSource provideLocalDataSource(Application context) {
        return new LocalDataSourceImp(context);
    }

    @Provides
    @Singleton
    public MailRepository provideMailRepository(LocalDataSource localDataSource, NetworkDataSource networkDataSource) {
        return new MailRepositoryImp(localDataSource, networkDataSource);
    }

    @Provides
    @Singleton
    public IScheduler provideScheduler() {
        return new WorkScheduler();
    }
}
