package com.example.masuikvova.tempmail.di;

import com.example.masuikvova.tempmail.helper.IScheduler;
import com.example.masuikvova.tempmail.repository.MailRepository;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Vova on 25.02.2018.
 */

@Singleton
@Component(modules = {MailModule.class})
public interface AppComponent {
    MailRepository provideRepository();

    IScheduler provideScheduler();
}
