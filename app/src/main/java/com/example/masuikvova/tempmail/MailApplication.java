package com.example.masuikvova.tempmail;

import android.app.Application;
import android.content.Context;
import android.support.v7.app.AppCompatDelegate;

import com.example.masuikvova.tempmail.di.AppComponent;
import com.example.masuikvova.tempmail.di.DaggerAppComponent;
import com.example.masuikvova.tempmail.di.MailModule;
import com.example.masuikvova.tempmail.di.PresenterComponent;

/**
 * Created by Vova on 21.02.2018.
 */

public class MailApplication extends Application {

    private AppComponent appComponent;
    private PresenterComponent presenterComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        initAppComponent();
    }

    public static MailApplication get(Context context) {
        return (MailApplication) context.getApplicationContext();
    }

    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .mailModule(new MailModule(this))
                .build();
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }

    public PresenterComponent getPresenterComponent(){
        return presenterComponent;
    }
}
