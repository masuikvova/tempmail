package com.example.masuikvova.tempmail.repository.LocalData;

import com.example.masuikvova.tempmail.models.Account;

/**
 * Created by Vova on 21.02.2018.
 */

public interface LocalDataSource {

    Account getAccount();

    void saveAccount(Account account, boolean withName);

    void saveExpirationTime(long time);

    long getExpirationTime();

    void clearUserData();
}
