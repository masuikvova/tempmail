package com.example.masuikvova.tempmail.di;

import javax.inject.Scope;

/**
 * Created by Vova on 27.02.2018.
 */

@Scope
public @interface FragmentScope {
}
