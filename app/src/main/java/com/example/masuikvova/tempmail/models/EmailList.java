package com.example.masuikvova.tempmail.models;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vova on 21.02.2018.
 */

public class EmailList extends BaseModel {

    private List<Email> emailList;

    @NonNull
    public List<Email> getEmails() {
        if (emailList == null) {
            emailList = new ArrayList<>();
        }
        return emailList;
    }

    public void setEmailList(List<Email> emailList) {
        this.emailList = emailList;
    }
}
