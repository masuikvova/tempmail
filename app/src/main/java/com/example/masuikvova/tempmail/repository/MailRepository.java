package com.example.masuikvova.tempmail.repository;

import com.example.masuikvova.tempmail.models.Account;
import com.example.masuikvova.tempmail.models.Content;
import com.example.masuikvova.tempmail.models.Email;
import com.example.masuikvova.tempmail.models.Status;

import java.util.List;

import rx.Observable;

/**
 * Created by Vova on 22.02.2018.
 */

public interface MailRepository {

    Account getAccount();

    long getExpirationTime();

    Observable<Account> createMainBox();

    Observable<Account> createMainBox(String mailBoxName);

    Observable<Status> clearMailBox(String key);

    Observable<Status> deleteMailBox(String key);

    Observable<Status> getExpirationTime(String key);

    Observable<Status> updateTime(String key);

    Observable<List<Email>> getEmailList(String key);

    Observable<Content> getMailContent(String key, int id);
}
