package com.example.masuikvova.tempmail.repository.api;

import com.example.masuikvova.tempmail.models.Email;
import com.example.masuikvova.tempmail.models.EmailList;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;


public class MailListDeserializer implements JsonDeserializer<EmailList> {
    @Override
    public EmailList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        EmailList emailList = new EmailList();
        if (json.isJsonArray()) {
            Type responseType = new TypeToken<List<Email>>() { }.getType();
            emailList.setEmailList(context.deserialize(json, responseType));
        } else {
            JsonObject object = json.getAsJsonObject();
            emailList.setErrorMsg(object.get("error").getAsString());
        }
        return emailList;
    }
}
